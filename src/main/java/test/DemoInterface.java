package test;

/**
 * @ClassName DemoInterface
 * @Description: TODO
 * @Author LiMinghua
 * @Date 2021/3/23
 * @Version V1.0
 **/
public interface DemoInterface {
    public void test();
}
