package test;

import java.math.BigInteger;

/**
 * @ClassName Outer
 * @Description: TODO
 * @Author LiMinghua
 * @Date 2021/3/23
 * @Version V1.0
 **/
public class Outer {
    private int age = 20;
    private BigInteger agr0 = BigInteger.valueOf(10);

    public Inner getInner(){
        return new Inner();
    }

    public void doSomething(DemoInterface demo){
        System.out.println("test");
        demo.test();
    }

    public class Inner{
        private Inner(){

        }
        public void show(){
            System.out.println(age);
        }
    }

    public static void main(String[] args) {
        Outer.Inner inner = new Outer().new Inner();
        inner.show();
        Outer.Inner inner1 = new Outer().getInner();
        inner1.show();
        Outer outer = new Outer();
        outer.doSomething(new DemoInterface() {
            @Override
            public void test() {
                System.out.println("hello");
            }
        });

    }
}


