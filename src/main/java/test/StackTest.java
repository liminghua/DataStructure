package test;
import java.util.Stack;

/**
 * @ClassName: StackTest
 * @ProjectName DataStructure
 * @Description: 测试栈结构/leetCode 1047. 删除字符串中的所有相邻重复项
 * @Author minghua
 * @Date 2021/3/93:23 下午
 * @Version V1.0
 */

public class StackTest {
    public static void main(String[] args) {
         Stack<Integer>  stack = new Stack<Integer>();
         stack.push(1);
         stack.push(2);
         while(!stack.empty()){
            System.out.println(stack.pop());
         }
         String str = "abbbbba";
         char[] chars = str.toCharArray();
         Outer outer = new Outer();
         //Outer.Inner inner = new Outer().new Inner();
         Outer.Inner inner = new Outer().getInner();


//        //分解步骤
//        char[] temp = new char[chars.length-2];
//        for (int i = 0; i <chars.length-1 ; i++) {
//            if(chars[i]==chars[i+1]){
//                for (int j = i; j < chars.length-2 ; j++) {
//                    chars[j] = chars[j+2];
//                    System.arraycopy(chars,0,temp,0,chars.length-2);
//                }
//            }
//        }
//
//        for (char s:temp
//             ) {
//            System.out.print(s);
//        }


        for (char s:deleteRepeat(chars)
             ) {
            System.out.print(s);
        }
    }

    public static char[] deleteRepeat(char[] chars){
        //base case 跳出递归的条件，数组全删完了 或者只剩下一位
        if(chars.length == 0 || chars.length == 1){
            return chars;
        }
        //存放删除一组重复元素后的数组
        char[] temp = new char[chars.length-2];
        //循环当前参数数组
        for (int i = 0; i <chars.length-1; i++) {
            //出现重复元素
            if(chars[i]==chars[i+1]){
                //删除重复元素
                for (int j = i; j < chars.length-2 ; j++) {
                    chars[j] = chars[j+2];
                }
                System.arraycopy(chars,0,temp,0,chars.length-2);
                //递归判断剩下的元素是否存在重复元素并继续删除
                return deleteRepeat(temp);
            }
        }
        return chars;
    }
}
